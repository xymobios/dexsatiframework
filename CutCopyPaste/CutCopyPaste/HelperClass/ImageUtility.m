//
//  ImageUtility.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "ImageUtility.h"
#import "ConstantString.h"

@implementation ImageUtility
+ (NSString*)pathForDocumentResource: (NSString*)resourcePath
{
    if(!resourcePath || ![resourcePath length])
        return nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths objectAtIndex: 0] stringByAppendingPathComponent: resourcePath];
}

//--------------------------------------------------------------------
// path for main bundle resources
//--------------------------------------------------------------------

+ (NSString*)pathForMainBundleResource: (NSString*)resourcePath
{
    if(!resourcePath || ![resourcePath length])
        return nil;
    
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: resourcePath];
}

+ (void)createChacheDirectory:(NSString *)kCacheFolderName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [self pathForDocumentResource: kCacheFolderName];
    
    if (![fileManager fileExistsAtPath: cachePath])
    {
        [fileManager createDirectoryAtPath: cachePath
               withIntermediateDirectories: NO
                                attributes: nil
                                     error: nil];
    }
}
+ (NSString *)saveImage: (UIImage*)image savedPath:(NSString *)path  imageInfo:(NSString *)editImageName
{
    if (image != nil)
    {
        [self createChacheDirectory:path];
        GetAppDelegate().imageCounter++;
        
        NSString *imageName = editImageName;
        if(editImageName == nil)
        {
            imageName = [self imageNameFromDate];
        }
        
        NSString *dirPath = [self pathForDocumentResource: path];
        dirPath = [dirPath stringByAppendingPathComponent: imageName];
        NSData* data = UIImagePNGRepresentation(image);        
       [data writeToFile:dirPath atomically:YES];
        return imageName;
    }
    return nil;    
}

+ (NSMutableArray *)getListOfSavedImage:(BOOL)animated path:(NSString *)imagePath
{
    NSMutableArray  *arrayOfImages = [[NSMutableArray alloc]init];
    NSError *error = nil;
    
    NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    stringPath = [NSString stringWithFormat:@"%@/%@",stringPath,imagePath];
    NSArray *filePathsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: stringPath  error:&error];
    //NSLog( @"File Path %@",filePathsArray);
   // NSLog( @"String  Path %@",stringPath);
   // NSLog( @"File Path %@",filePathsArray);
    for(int i=0;i<[filePathsArray count];i++)
    {
        NSString *strFilePath = [filePathsArray objectAtIndex:i];
        if ([[strFilePath pathExtension] isEqualToString:@"jpg"] || [[strFilePath pathExtension] isEqualToString:@"JPG"] || [[strFilePath pathExtension] isEqualToString:@"png"] || [[strFilePath pathExtension] isEqualToString:@"PNG"])
        {
            NSString *imagePath = [[stringPath stringByAppendingString:@"/"] stringByAppendingString:strFilePath];
            NSString *nameWithoutExtention = [strFilePath stringByDeletingPathExtension];
            
            nameWithoutExtention = [Utility dateFromatConverter:nameWithoutExtention currentFromat:kImageNameSaved newFormat:kImageNameDisplay];
            NSLog(@"Path Of Image %@",nameWithoutExtention );
            NSData *data = [NSData dataWithContentsOfFile:imagePath];
            if(data)
            {
                UIImage *image = [UIImage imageWithData:data];
                NSDictionary *imageInfo = [NSDictionary dictionaryWithObjectsAndKeys:nameWithoutExtention,kImageName,image,kImageData, strFilePath, kImageKey, nil];
                
                [arrayOfImages addObject:imageInfo];
            }
        }
    }
    arrayOfImages  = (NSMutableArray *)[[arrayOfImages reverseObjectEnumerator] allObjects];
    return arrayOfImages;
}


+ (NSString *) imageNameFromDate
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd HH:mm:ss a"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    return  [NSString stringWithFormat:@"%@.png",dateString];
}


+(NSString *)displayImageName :(NSString *)imageName
{
    DebugLog(@"Before name %@",imageName);
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"MMMM dd, yyyy";
    NSDate* date = [formatter dateFromString:imageName];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd HH:mm a"];
    NSString *dateString = [format stringFromDate:date];
    DebugLog(@"After name %@",dateString);
    return dateString;
}

+ (void)removeCachedImageForKey: (NSString*) key path:(NSString *)path
{
    if (key != nil)
    {
        NSError *error = nil;
        NSString *dirPath = [self pathForDocumentResource: path];
        dirPath = [dirPath stringByAppendingPathComponent: key];
        NSFileManager *manager = [NSFileManager defaultManager];
        
        [manager removeItemAtPath: dirPath error: &error];
    }
}


+ (NSDictionary  *)getImageFromKey:(NSString *)key imageData:(UIImage *)image
{
    DebugLog(@"Image Detail %@",key);
    NSString *nameWithoutExtention = [key stringByDeletingPathExtension];
    NSString *imageName = [Utility dateFromatConverter:nameWithoutExtention currentFromat:kImageNameSaved newFormat:kImageNameDisplay];    
    
        NSDictionary *imageInfo = [NSDictionary dictionaryWithObjectsAndKeys:imageName,kImageName,image,kImageData, key, kImageKey, nil];
    
    DebugLog(@" image Name %@",imageInfo);
        return imageInfo;
}

@end
