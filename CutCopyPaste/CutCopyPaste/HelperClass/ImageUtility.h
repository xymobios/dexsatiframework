//
//  ImageUtility.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Utility.h"

@interface ImageUtility : NSObject


+ (NSString*)pathForDocumentResource: (NSString*)resourcePath;
+ (NSString*)pathForMainBundleResource: (NSString*)resourcePath;
+ (void)createChacheDirectory:(NSString *)kCacheFolderName;
+ (NSString *)saveImage: (UIImage*)image savedPath:(NSString *)path  imageInfo:(NSString *) imageInfo;
+ (NSMutableArray *)getListOfSavedImage:(BOOL)animated path:(NSString *)imagePath;

+ (NSString *) imageNameFromDate;
+ (void)removeCachedImageForKey: (NSString*) key path:(NSString *)path;
+ (NSDictionary  *)getImageFromKey:(NSString *)key imageData:(UIImage *)image;

@end
