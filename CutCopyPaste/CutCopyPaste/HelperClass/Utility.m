//
//  Utility.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "Utility.h"
#import "AppDelegate.h"



AppDelegate* GetAppDelegate()
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
@implementation Utility

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
{
    
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    [GetAppDelegate().alertView show];
}

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
                      tagValue:(int)tag
{
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = nil;
    GetAppDelegate().alertView	= [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    
    [GetAppDelegate().alertView setTag:tag];
    
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    
    [GetAppDelegate().alertView show];
}
+ (NSString*)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}



+ (CGFloat)deviceWidth
{
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat)deviceHeight
{
    return [UIScreen mainScreen].bounds.size.height;
}
+ (CGFloat)getXpoint:(CGFloat)width frameWidth:(CGFloat)frameWidth
{
    return (frameWidth - width)/2;
}

+ (CGFloat)getYpoint:(CGFloat)height frameHeight:(CGFloat)frameHeight
{
    return -(frameHeight - height)/2;
}


+ (CGFloat)getYpointForViewController:(CGFloat)height frameHeight:(CGFloat)frameHeight
{
       return (frameHeight - height)/2;
}



+ (void)resizeViewForiPhoneAndiPod:(UIView*)targetView
{
    // if device is iPad then we dont need to set frame size for target view
    //if ([Helper isDeviceIPad])  return;
    if([Utility isDeviceIPad] && [[[UIDevice currentDevice] systemVersion] doubleValue] < 8.0)
    {
        return;
    }
    // this frame setting is for iPhone 5 and other iPhone/iPodTouch devices
    targetView.frame =  CGRectMake(targetView.frame.origin.x,
                                   targetView.frame.origin.y,
                                   [[UIScreen mainScreen]bounds].size.width,
                                   [[UIScreen mainScreen]bounds].size.height);
}

+ (BOOL) isDeviceIPad
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
#endif
    return NO;
}

+ (NSString*)dateFromatConverter:(id) date currentFromat:(NSString *)currentFromat newFormat:(NSString *) newFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:currentFromat];
    NSDate *newDate = [dateFormatter dateFromString:date];
    [dateFormatter setDateFormat:newFormat];
    NSString *formattedStringDate = [dateFormatter stringFromDate:newDate];
    return formattedStringDate;
}

+ (BOOL)cutMagnifyingGlassStatus
{
   if([[NSUserDefaults standardUserDefaults] integerForKey:kCutMFGlass] == ENUM_MAGNIFYING_GLASS_CUT_OFF)
   {
       return NO;
   }
    return YES;
}
+ (BOOL)advanceEditMagnifyingGlassStatus
{
    if([[NSUserDefaults standardUserDefaults] integerForKey:kAdEditMFGlass] == ENUM_ADVANCE_MAGNIFYING_GLASS_OFF)
    {
        return NO;
    }
    return YES;
}
+ (UIColor *)getCurrentColor
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:kCutLineColorKey])
    {
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:kCutLineColorKey];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        return color;
    }
    return kCutDefaultColor;
}

+ (UIColor *)setCurrentColor:(UIColor *)currentColor
{
     NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:currentColor];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:kCutLineColorKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return currentColor;
}

+ (BOOL)isIOS8
{
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
