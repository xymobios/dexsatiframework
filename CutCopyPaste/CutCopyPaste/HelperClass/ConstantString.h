//
//  ConstantString.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#ifndef DexsatiFramwork_ConstantString_h
#define DexsatiFramwork_ConstantString_h

enum ALERT_VIEW_TAG
{
    ENUM_TAG_PICTURE_SET = 1,
    ENUM_TAG_CANCEL = 2,
    ENUM_TAG_FOR_CAMERA = 3,
    ENUM_TAG_FOR_GALERY = 4
};

enum ADVANCE_EDIT_MODE
{
    ENUM_OPEN_FOROM_CUT_SCREEN = 1,
    ENUM_OPEN_SELECT_SCREEN = 2,    
};



enum
{
    ENUM_NOT_DRAW = 0,
    ENUM_IMAGE_DRAW = 1,
    ENUM_MOVING =2 ,
    ENUM_CLIPPED = 3,
    ENUM_FINISHED = 4,
    ENUM_CLEAR_IMAGE = 5,
    ENUMT_TRANSPARENT_IMAGE = 6
    
};

#define kSettingsMessageGallery @"Please go to Privacy and change the settings to use Gallery"
#define kSettingsMessageCamera @"Please go to Privacy and change the settings to use Camera"
#define kNoCameraMessage   @"Device has no camera"
#define kSelectImageMsg    @"Please select at least one image"
#define kCreationemptyMsg  @"You have not created any Pasted Photos or Collages yet. Please create content."
#define kCollageAndPasteEmptyMSg @"No Cut Photos to Paste on Other Photos. Please Cut the Photos first to Paste on other photos."
#define kAdvaceEditEmptyMSg @"No Cut Photos to Edit. Please Cut the Photos first to fine tune the Edits."


#define kOK             @"OK"
#define kWarning        @"Warning"
#define kError          @"Error"
#define kSorry          @"Sorry"
#define kClose          @"Close"
#define kYes            @"Yes"
#define kNo             @"No"
#define kCancel         @"Cancel"

#define kCreationPath   @"Creation"
#define kCroping        @"Croping"
#define kAdvanceEraser @"AdvanceEraser"

#define kImageNameSaved     @"MMM dd HH:mm:ss a"
#define kImageNameDisplay   @"MMM dd hh:mm a"

//Image Info
#define kImageName @"imageName"
#define kImageKey  @"imageKey"
#define kImageData @"imageData"

//Sharing content

#define kShareContent @"Say something about this photo"



//Default line width for cut screen
#define kCutDefaultLineWidth 5
#define kCutDefaultColor [UIColor blackColor]
#define kCutLineColorKey @"lineColor"
#define kEditAdvanceDefaultLineWidth 5


// Magnifying Glass Constant
#define kAdEditMFGlass @"EditMagnifyingGlassKey"
#define kCutMFGlass    @"CutMagnifyingGlassKey"

#define kMagnifyScale 2.5

enum MAGNIFYING_GLASS_AD_EDIT_MODE
{
    ENUM_ADVANCE_MAGNIFYING_GLASS_NORMAL = 100,
    ENUM_ADVANCE_MAGNIFYING_GLASS_ON = 101,
    ENUM_ADVANCE_MAGNIFYING_GLASS_OFF = 102
    
};

enum MAGNIFYING_GLASS_CUT_MODE
{
    ENUM_MAGNIFYING_GLASS_CUT_NORMAL = 200,
    ENUM_MAGNIFYING_GLASS_CUT_ON = 201,
    ENUM_MAGNIFYING_GLASS_CUT_OFF = 202
    
};


#endif
