//
//  YourCreationViewController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 05/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourCreationViewController : UIViewController
{
    NSMutableArray *createdImage;
    IBOutlet UICollectionView *creationViews;
}
@end
