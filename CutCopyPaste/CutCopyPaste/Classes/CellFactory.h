//
//  CellFactory.h
//
//  Created by Andrew Karpiuk on 4/21/09.
//  Copyright 2009 DAVA Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"



@interface CellFactory : NSObject
{
    NSMutableDictionary	*cellStore;
	NSMutableDictionary	*frameStore;
}

+ (CellFactory*)sharedCellFactory;

- (UITableViewCell*)cellOfKind:(NSString*)cellKind
                      forTable:(UITableView*)tableView
                withBackground:(NSString*)backgroundName;

- (CGRect)frameForCellOfKind:(NSString*)cellKind;

@end 