//
//  CropperViewController.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CropperView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ConstantString.h"
#import "UIImage+OrientationFix.h"
#import "CameraGalleryController.h"

@interface CropperViewController : UIViewController <CropperViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, CameraGalleryDelegate>

{
    CameraGalleryController *controller;
    UIImage *pickerImage;
}
@property(nonatomic, strong) CropperView *cropView;
@property(nonatomic, strong) IBOutlet UIView *fadeView;
@property(nonatomic, strong) IBOutlet UIView *croppedImageView;
@property(nonatomic, strong) IBOutlet UIImageView *cropedImageContainer;

@property(nonatomic, strong) UIImage *croppedImage;
- (IBAction)takePhoto:  (UIButton *)sender;
- (IBAction)selectPhoto:(UIButton *)sender;
- (void)selectPicture;
@end
