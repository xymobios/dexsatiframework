//
//  PastePhotoViewController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 02/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DisplayViewController.h"
#import "CameraGalleryController.h"
@interface PastePhotoViewController : DisplayViewController<CameraGalleryDelegate,UIImagePickerControllerDelegate, UIAlertViewDelegate,UINavigationControllerDelegate>
{
     CameraGalleryController *controller;
     IBOutlet UIView *fadeView;
}
- (void)selectPicture;
@end
