//
//  SelectAdvanceEditController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectAdvanceEditController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *croppedImages;
    IBOutlet UICollectionView *croppedImageCollectionView;
}
    @end
