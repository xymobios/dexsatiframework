//
//  AppDelegate.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "Utility.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
   //_stickerImage = [UIImage imageNamed:@"DSC06267.JPG"];
    
    
    if([[NSUserDefaults standardUserDefaults] integerForKey:kCutMFGlass] == 0 )
    {
        [[NSUserDefaults standardUserDefaults] setInteger:ENUM_MAGNIFYING_GLASS_CUT_NORMAL forKey:kCutMFGlass];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    if([[NSUserDefaults standardUserDefaults] integerForKey:kAdEditMFGlass] == 0 )
    {
        [[NSUserDefaults standardUserDefaults] setInteger:ENUM_ADVANCE_MAGNIFYING_GLASS_NORMAL forKey:kCutMFGlass];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    _mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:_mainViewController];
       [_window setRootViewController:navigation];
    
    navigation.navigationBar.hidden = YES;
    [_window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (self.alertView.visible)
    {
        [self.alertView dismissWithClickedButtonIndex:0 animated:NO];
    }
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)showLoadingViewWithMsg:(NSString *)_message
{
    if(([Utility isDeviceIPad] && [Utility isIOS8]) || ![Utility isDeviceIPad])
    {
        [Utility resizeViewForiPhoneAndiPod:loadingView];
        messageView.layer.cornerRadius = 6;
        messageView.center = loadingView.center;
    }
    message.text = _message;
    [self.window addSubview:loadingView];
}

- (void)hideLoadingView
{
    [loadingView removeFromSuperview];
}

- (void)showFadeView
{
    if(([Utility isDeviceIPad] && [Utility isIOS8]) || ![Utility isDeviceIPad])
    {
        [Utility resizeViewForiPhoneAndiPod:fadeView];
    }
    [self.window addSubview:fadeView];
}

- (void)hideFadeView
{
    [fadeView removeFromSuperview];
}


@end
