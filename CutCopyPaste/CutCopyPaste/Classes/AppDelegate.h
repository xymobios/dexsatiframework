//
//  AppDelegate.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

{
    IBOutlet UIView *loadingView;
    IBOutlet UIView *fadeView;
    IBOutlet UIView *messageView;
    IBOutlet UILabel *message;
}

@property (assign, nonatomic) NSInteger imageCounter;
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (nonatomic,strong) UIAlertView *alertView;
@property (nonatomic,assign) BOOL cutMagnifyStatus;
@property (nonatomic,assign) BOOL editMagnifyStatus;


- (void)showLoadingViewWithMsg:(NSString *)_message;
- (void)hideLoadingView;
- (void)showFadeView;
- (void)hideFadeView;


@end

