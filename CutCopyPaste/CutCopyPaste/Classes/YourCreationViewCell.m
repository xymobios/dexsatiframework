//
//  YourCreationViewCell.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 05/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "ALAssetsLibrary+CustomPhotoAlbum.h"

#import "YourCreationViewCell.h"



@implementation YourCreationViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = 5;
    self.layer.borderColor =[UIColor whiteColor].CGColor;
}

@end
