//
//  CropperView.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CropperView.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "UIImage+SimpleResize.h"
#import "MagnifierView.h"

@implementation CropperView

@synthesize controllerImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    switch (status) {
            
        case ENUM_CLEAR_IMAGE:
        {
            CGContextClearRect(context, self.bounds);
        }
            break;
        case ENUM_IMAGE_DRAW:
        {
            orignalImage = controllerImage;
           
            //orignalImage = [UIImage imageNamed:@"DSC06267.JPG"];
            CGRect imageRect = CGRectMake([Utility getXpoint:orignalImage.size.width frameWidth:self.frame.size.width] , [Utility getYpoint:orignalImage.size.height frameHeight:self.frame.size.height],orignalImage.size.width, orignalImage.size.height);
            
                   
            //CGRect imageRect = CGRectMake(0, 0, orignalImage.size.width,orignalImage.size.height);
            CGContextTranslateCTM(context, 0, imageRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, orignalImage.CGImage);
            
        }
            break;
        case ENUM_MOVING:
        {
            orignalImage = controllerImage;
            // orignalImage = [UIImage imageNamed:@"DSC06267.JPG"];
            //CGRect imageRect = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height);
            
            CGRect imageRect = CGRectMake([Utility getXpoint:orignalImage.size.width frameWidth:self.frame.size.width] , [Utility getYpoint:orignalImage.size.height frameHeight:self.frame.size.height],orignalImage.size.width, orignalImage.size.height);
           
            CGContextTranslateCTM(context, 0, imageRect.size.height);
           CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, orignalImage.CGImage);
            if(points.count > 0)
            {
                
                CGContextBeginPath(context);
                CGFloat dash[2]={2 ,3 };
                CGPoint myStartPoint= [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                CGContextMoveToPoint(context, myStartPoint.x,orignalImage.size.height- myStartPoint.y);
                
                for (int j=0; j<[points count]-1; j++)
                {
                    CGPoint myEndPoint = [(NSValue *)[pointsPath objectAtIndex:j+1] CGPointValue];
                    //--------------------------------------------------------
                    CGContextSetLineDash(context,0,dash,2);
                    CGContextAddLineToPoint(context, myEndPoint.x,orignalImage.size.height-myEndPoint.y);
                }
                
                UIColor *lineColor= [Utility getCurrentColor];
                float width=kCutDefaultLineWidth;
                CGContextSetStrokeColorWithColor(context,[lineColor CGColor]);
                //-------------------------------------------------------
                CGContextSetLineWidth(context, width);
                CGContextStrokePath(context);
            }
            
        }
            break;
        case ENUM_CLIPPED:
        {
            if (pointsPath && pointsPath.count > 0) {
                CGPoint p = [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                
                p = CGPointMake(p.x, p.y);
                
                CGPathMoveToPoint(myPath, nil, p.x, p.y);
                
                for (int i = 1; i < pointsPath.count; i++) {
                    p = [(NSValue *)[pointsPath objectAtIndex:i] CGPointValue];
                    p = CGPointMake(p.x, p.y);
                    CGPathAddLineToPoint(myPath, nil, p.x,  p.y);
                }
            }
            CGContextAddPath(context, myPath);
            CGContextClip(context);
            CGPathRelease(myPath);
            [orignalImage drawInRect:[self bounds]];
            
        }
            break;
        case ENUM_FINISHED:
        {
            //NSLog(@"CropRect x= %f top =%f width = %f height = %f",cropRect.origin.x,cropRect.origin.y,cropRect.size.width,cropRect.size.height);
            
            //CGContextAddRect(context, cropRect);
            //UIColor *lineColor=[UIColor blackColor];
            //float width=3.0;
            //CGContextSetStrokeColorWithColor(context,[lineColor CGColor]);
            //-------------------------------------------------------
            //CGContextSetLineWidth(context, width);
            //CGContextStrokeRect(context, cropRect);
            
            
            UIGraphicsBeginImageContextWithOptions(orignalImage.size, NO, 0.0);
            CGContextBeginPath (context);
            CGFloat height = orignalImage.size.height;
            
            
            CGContextTranslateCTM(context, 0.0, height);
            CGContextScaleCTM(context, 1.0, -1.0);
            
            if (pointsPath && pointsPath.count > 0) {
                CGPoint p = [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                
                p = CGPointMake(p.x, p.y);
                
                CGPathMoveToPoint(myPath, nil, p.x, p.y);
                
                for (int i = 1; i < pointsPath.count; i++) {
                    p = [(NSValue *)[pointsPath objectAtIndex:i] CGPointValue];
                    p = CGPointMake(p.x, p.y);
                    CGContextSetLineCap(context, kCGLineCapButt);
                    // pattern 6 times “solid”, 5 times “empty”
                   
                    CGPathAddLineToPoint(myPath, nil, p.x,  p.y);
                    
                }
            }
          
            CGContextAddPath(context, myPath);
            CGContextSetBlendMode (context, kCGBlendModeNormal);
            //            UIBezierPath *tapTarget = [UIBezierPath bezierPathWithCGPath:myPath];
//           // CGPathRelease(tapTargetPath);
            CGContextClosePath(context);
            CGContextSaveGState(context);
           // CGContextClip(context);
            
            maskLayer = [CAShapeLayer layer];
            //CGPathRef path = myPath;
            maskLayer.path = myPath;            // Release the path since it's not covered by ARC.
            // Set the mask of the view.
            self.layer.mask = maskLayer;
           cropRect=  CGPathGetBoundingBox(myPath);           
            UIImage *maskedImage= [self imageFromImageView :self];
            
            NSLog(@"CropRect x= %f top =%f width = %f height = %f",cropRect.origin.x,cropRect.origin.y,cropRect.size.width,cropRect.size.height);
            DebugLog(@"maskedImagewidth %f maskedImage height %f",maskedImage.size.width, maskedImage.size.height);
            
            CGImageRef clipRef  = CGImageCreateWithImageInRect(maskedImage.CGImage, cropRect);
            clippedImage = [UIImage imageWithCGImage:clipRef];
            DebugLog(@"width %f height %f",clippedImage.size.width, clippedImage.size.height);
            if([_delegate respondsToSelector:@selector(croppedImageWithPath:image:)])
            {
                [_delegate croppedImageWithPath:myPath image:clippedImage];
            }
        }
            
            break;
            
        case ENUMT_TRANSPARENT_IMAGE:
        {
            UIGraphicsBeginImageContextWithOptions(cropRect.size, NO, 0.0);
            UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            CGContextDrawImage(context, CGRectMake(cropRect.origin.x,cropRect.origin.y,cropRect.size.width, cropRect.size.height), blank.CGImage);
        }
            break;
            
        default:
        {
            return;
        }
            break;
    }
}

#pragma mark- **Touch Events**
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    points = [NSMutableArray array];
    UITouch *touch = [touches anyObject];
    pointsPath = [NSMutableArray array];
    CGPoint touchPosition = [touch locationInView:self];
    [pointsPath addObject:[NSValue valueWithCGPoint:touchPosition]];
   
    if([Utility cutMagnifyingGlassStatus])
    {
        self.touchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(addLoop) userInfo:nil repeats:NO];    
    // just create one loop and re-use it.
    if (loop == nil)
        {
            loop = [[MagnifierView alloc] init];
            loop.viewToMagnify = self;
        }
        loop.touchPoint = [touch locationInView:self];
        [loop setNeedsDisplay];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch=[touches anyObject];
    myPath = CGPathCreateMutable();
    CGPoint touchPosition = [touch locationInView:self];
    //[self setCropRect:touchPosition];
    
    [points addObject:[NSValue valueWithCGPoint:touchPosition]];
    [pointsPath addObject:[NSValue valueWithCGPoint:touchPosition]];
    status = ENUM_MOVING;
    [self setNeedsDisplay];
    if([Utility cutMagnifyingGlassStatus])
    {
        [self handleAction:touches];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{  
    status = ENUM_FINISHED;
    [self setNeedsDisplay];
    points = nil;
    if([Utility cutMagnifyingGlassStatus])
    {
        [self.touchTimer invalidate];
        self.touchTimer = nil;
        [loop removeFromSuperview];
    }
}


#pragma mark - **User Define Methods**
-(void)clear
{
    [points removeAllObjects];
    [pointsPath removeAllObjects];
    
    //CGPathRelease(myPath);
    status=ENUM_CLEAR_IMAGE;
   // cropRect.size = self.bounds.size;
    self.layer.mask = nil;
    [self setNeedsDisplay];
}

-(void)setNewImage
{
    orignalImage = nil;
    orignalImage = controllerImage;
    status=ENUM_IMAGE_DRAW;
    [self setNeedsDisplay];
}

//-(void)setCliipedImage
//{
////    status=ENUM_TEST_CODE;
////    orignalImage = controllerImage;
////    [self setNeedsDisplay];
//}

- (UIImage *)maskImage:(UIImage *)originalImage toPath:(UIBezierPath *)path {
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.0);
    [path addClip];
    
    UIImage *maskedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return maskedImage;
}

- (UIImage*)imageFromImageView:(UIView *)imageView
{
    UIGraphicsBeginImageContext(self.bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextRotateCTM(context, 2*M_PI);
    
    [imageView.layer renderInContext:context];
    UIImage *image =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if([Utility cutMagnifyingGlassStatus])
    {
        [self.touchTimer invalidate];
        self.touchTimer = nil;
        [loop removeFromSuperview];
    }
}


- (void)addLoop {
    // add the loop to the superview.  if we add it to the view it magnifies, it'll magnify itself!
    [self.superview addSubview:loop];
    // here, we could do some nice animation instead of just adding the subview...
}

- (void)handleAction:(id)timerObj {
    NSSet *touches = timerObj;
    UITouch *touch = [touches anyObject];
    loop.touchPoint = [touch locationInView:self];
    [loop setNeedsDisplay];
}


@end
