//
//  MagnifierView.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 11/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MagnifierView : UIView {
    UIView *viewToMagnify;
    CGPoint touchPoint;
}

@property (nonatomic, retain) UIView *viewToMagnify;
@property (assign) CGPoint touchPoint;

@end
