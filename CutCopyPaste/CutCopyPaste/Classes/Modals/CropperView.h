//
//  CropperView.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnifierView.h"

@protocol CropperViewDelegate;

@protocol CropperViewDelegate <NSObject>

- (void)croppedImageWithPath:(CGPathRef)path image:(UIImage *)image;

@end

@interface CropperView : UIView{
    NSMutableArray *points;    
    NSMutableArray *pointsPath;
    CGMutablePathRef myPath;
    BOOL isClipped;
    UIImage* orignalImage;
    UIImage* clippedImage;
    NSInteger status;
    float xmin, xmax, ymin,ymax;
    CGRect cropRect;
    
    // Magnify Glass Variable
    CAShapeLayer *maskLayer;
    MagnifierView *loop;
    
    
}
@property (weak, nonatomic) IBOutlet UIImage *controllerImage;
@property(nonatomic, strong) NSString *imageName;
@property (weak, nonatomic) id<CropperViewDelegate> delegate;
//Magnifier
@property (nonatomic, strong) NSTimer *touchTimer;

-(void)clear;
-(void)setNewImage;
//-(void)setCliipedImage;

 // Magnify Glass Variable   
- (void)addLoop;
- (void)handleAction:(id)timerObj;

@end
