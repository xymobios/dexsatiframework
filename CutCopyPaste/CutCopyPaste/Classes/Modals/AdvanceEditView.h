//
//  AdvanceEditView.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 10/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnifierView.h"

@interface AdvanceEditView : UIView
{    
    MagnifierView *loop;
}
@property (nonatomic, strong) NSNumber *widthLine;
@property (nonatomic, assign) UIImage *advanceImage;
@property (nonatomic, assign) NSInteger status;

@property (nonatomic, strong) NSTimer *touchTimer;
-(void)addLineArray;
-(void)undoOperation;
-(void)redoOperation;
//-(void)clearContext;
-(void)setSliderValue:(NSNumber *)width;

// Magnify Glass Variable
- (void)addLoop;
- (void)handleAction:(id)timerObj;

@end
