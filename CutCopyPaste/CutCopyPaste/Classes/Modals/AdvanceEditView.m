//
//  AdvanceEditView.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 10/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "AdvanceEditView.h"
#import "Utility.h"
@implementation AdvanceEditView


// Hold all the touch points
static NSMutableArray *pointArray;
// Hold each line after touch end.
static NSMutableArray *lineArray;
//Delete Line Array , when user click redo or undo operation
static NSMutableArray *deleteLineArray;
// Line Width
static NSMutableArray *widthArray;
//delete line width
static NSMutableArray *deleteWidthArray;

- (id)init
{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        widthArray=[[NSMutableArray alloc]init];
        deleteWidthArray=[[NSMutableArray alloc]init];
        pointArray=[[NSMutableArray alloc]init];
        lineArray=[[NSMutableArray alloc]init];
        deleteLineArray=[[NSMutableArray alloc]init];
        _widthLine =  [NSNumber numberWithInt: kEditAdvanceDefaultLineWidth];
        
        //[widthArray addObject:_widthLine];
    }
    return self;
}

- (void)setSliderValue:(NSNumber *)width
{
    _widthLine = width;
}
- (void)drawRect:(CGRect)rect
{
    DebugLog(@"Current Status %d",_status);
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextSetLineWidth(context, [_widthLine floatValue]);
    CGContextSetLineJoin(context,kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapButt);
    switch (_status)
    {
        case ENUM_CLEAR_IMAGE:
        {
            CGContextClearRect(context, self.bounds);
        }
            break;
            
        case ENUM_IMAGE_DRAW:
        {
            CGRect imageRect = CGRectMake([Utility getXpoint:_advanceImage.size.width frameWidth:self.frame.size.width] , [Utility getYpoint:_advanceImage.size.height frameHeight:self.frame.size.height],_advanceImage.size.width, _advanceImage.size.height);
            CGContextTranslateCTM(context, 0, imageRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, _advanceImage.CGImage);
            
        }
            break;
        case ENUM_MOVING:
        {
            CGRect imageRect = CGRectMake([Utility getXpoint:_advanceImage.size.width frameWidth:self.frame.size.width] , [Utility getYpoint:_advanceImage.size.height frameHeight:self.frame.size.height],_advanceImage.size.width, _advanceImage.size.height);
            
            //CGRect imageRect = CGRectMake(0, 0, orignalImage.size.width,orignalImage.size.height);
            CGContextTranslateCTM(context, 0, imageRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, _advanceImage.CGImage);
            if ([lineArray count]>0) {
                for (int i=0; i<[lineArray count]; i++) {
                    NSArray * array=[NSArray
                                     arrayWithArray:[lineArray objectAtIndex:i]];
                    
                    if ([array count]>0)
                    {
                        CGContextBeginPath(context);
                        CGPoint myStartPoint=CGPointFromString([array objectAtIndex:0]);
                        CGContextMoveToPoint(context, myStartPoint.x, _advanceImage.size.height - myStartPoint.y);
                        
                        for (int j=0; j<[array count]-1; j++)
                        {
                            CGPoint myEndPoint=CGPointFromString([array objectAtIndex:j+1]);
                            //--------------------------------------------------------
                            CGContextAddLineToPoint(context, myEndPoint.x,_advanceImage.size.height -myEndPoint.y);
                        }
                        
                        float width=[_widthLine floatValue];
                        if(widthArray.count != 0)
                        {
                            NSNumber *wid=[widthArray objectAtIndex:i];
                            width = [wid floatValue];
                        }
                        
                        CGContextSetLineWidth(context, width);
                        CGContextSetBlendMode(context,kCGBlendModeClear);
                        CGContextStrokePath(context);
                    }
                }
            }
            
            if ([pointArray count]>0)
            {
                CGContextBeginPath(context);
                CGPoint myStartPoint=CGPointFromString([pointArray objectAtIndex:0]);
                CGContextMoveToPoint(context, myStartPoint.x, _advanceImage.size.height - myStartPoint.y);
                
                for (int j=0; j<[pointArray count]-1; j++)
                {
                    CGPoint myEndPoint=CGPointFromString([pointArray objectAtIndex:j+1]);
                    //--------------------------------------------------------
                    CGContextAddLineToPoint(context, myEndPoint.x,_advanceImage.size.height-myEndPoint.y);
                }
                // UIColor *lineColor=[colors objectAtIndex:colorCount];
                //float width=lineWidthArray[widthCount];
                CGContextSetStrokeColorWithColor(context,[[UIColor redColor] CGColor]);
                
                // NSNumber *wid=[widthArray objectAtIndex:];
                //float width=[wid floatValue];
                //CGContextSetLineWidth(context, width);
                float width=[_widthLine floatValue];
                //-------------------------------------------------------
                CGContextSetLineWidth(context, width);
                CGContextSetBlendMode(context,kCGBlendModeClear);
                CGContextStrokePath(context);
            }
        }
            break;
        default:
        {
            return;
        }
    }
    
}

//add each Line and create new points


#pragma mark - Touch Delegates

static CGPoint MyBeganpoint;
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    if([Utility advanceEditMagnifyingGlassStatus])
    {
        
        self.touchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(addLoop) userInfo:nil repeats:NO];
        
        // just create one loop and re-use it.
        if (loop == nil) {
            loop = [[MagnifierView alloc] init];
            loop.viewToMagnify = self;
        }
        UITouch *touch = [touches anyObject];
        loop.touchPoint = [touch locationInView:self];
        [loop setNeedsDisplay];
    }
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    UITouch* touch=[touches anyObject];
    MyBeganpoint=[touch locationInView:self];
    NSString *sPoint=NSStringFromCGPoint(MyBeganpoint);
    [pointArray addObject:sPoint];
    _status = ENUM_MOVING;
    [self setNeedsDisplay];
    if([Utility advanceEditMagnifyingGlassStatus])
    {
        [self handleAction:touches];
    }
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    // _status = ENUM_FINISHED;
    if([Utility advanceEditMagnifyingGlassStatus])
    {
        [self addLineArray];
        [self.touchTimer invalidate];
        self.touchTimer = nil;
        [loop removeFromSuperview];
    }
    NSLog(@"touches end");
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    if([Utility advanceEditMagnifyingGlassStatus])
    {
        [self.touchTimer invalidate];
        self.touchTimer = nil;
        [loop removeFromSuperview];
    }
    NSLog(@"touches Canelled");
}


#pragma mark -User Define Methods
-(void)addLineArray
{
    if(_status == ENUM_NOT_DRAW)
    {
        return;
    }
    [widthArray addObject:_widthLine];
    
    NSArray *array=[NSArray arrayWithArray:pointArray];
    [lineArray addObject:array];
    pointArray=[[NSMutableArray alloc]init];
}

// Undo Graphics
#pragma mark - Undo and Redo Opertaions
- (void)undoOperation
{
    if ([lineArray count]) {
        [deleteLineArray addObject:[lineArray lastObject]];
        [lineArray removeLastObject];
    }
    if ([widthArray count]) {
        [deleteWidthArray addObject:[widthArray lastObject]];
        [widthArray removeLastObject];
    }
    [self setNeedsDisplay];
}
- (void)redoOperation
{
    if ([deleteLineArray count])
    {
        [lineArray addObject:[deleteLineArray lastObject]];
        [deleteLineArray removeLastObject];
    }
    if ([deleteWidthArray count])
    {
        [widthArray addObject:[deleteWidthArray lastObject]];
        [deleteWidthArray removeLastObject];
    }
    [self setNeedsDisplay];
}
//// Zoom option actvated
//- (void)clearContext
//{
//    //    [deleteLineArray removeAllObjects];
//    //    [lineArray removeAllObjects];
//    //    [pointArray removeAllObjects];
//    //    [deleteWidthArray removeAllObjects];
//    //    [WidthArray removeAllObjects];
//
//    _advanceImage = nil;
//    _status = ENUM_CLEAR_IMAGE;
//    [self setNeedsDisplay];
//    _status = ENUM_NOT_DRAW;
//}


- (void)addLoop {
    // add the loop to the superview.  if we add it to the view it magnifies, it'll magnify itself!
    [self.superview addSubview:loop];
    // here, we could do some nice animation instead of just adding the subview...
}

- (void)handleAction:(id)timerObj {
    NSSet *touches = timerObj;
    UITouch *touch = [touches anyObject];
    loop.touchPoint = [touch locationInView:self];
    [loop setNeedsDisplay];
}


@end
