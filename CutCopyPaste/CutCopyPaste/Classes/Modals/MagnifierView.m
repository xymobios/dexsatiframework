//
//  MagnifierView.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 11/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "MagnifierView.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#define circleRadius 140
@implementation MagnifierView
@synthesize viewToMagnify;
@dynamic touchPoint;

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame radius:circleRadius];
}

- (id)initWithFrame:(CGRect)frame radius:(int)r {
    int radius = r;
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, radius, radius)])) {
        //Make the layer circular.
        self.layer.cornerRadius = radius / 2;
        self.layer.masksToBounds = YES;
    }
    
    return self;
}

- (void)setTouchPoint:(CGPoint)pt {
    touchPoint = pt;
    // whenever touchPoint is set, update the position of the magnifier (to just above what's being magnified)
    
    float xPostion = circleRadius/2;
    if(pt.x < [Utility deviceWidth]/2)
    {
        xPostion = [Utility deviceWidth]- circleRadius/2;
    }
    self.center = CGPointMake(xPostion, [Utility deviceHeight]/2);
}

- (CGPoint)getTouchPoint {
    return touchPoint;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect bounds = self.bounds;
    CGImageRef mask = [UIImage imageNamed: @"loupe-mask@2x.png"].CGImage;
    UIImage *glass = [UIImage imageNamed: @"loupe-hi@2x.png"];
    
    CGContextSaveGState(context);
    CGContextClipToMask(context, bounds, mask);
    CGContextFillRect(context, bounds);
   
    CGContextTranslateCTM(context,1*(self.frame.size.width*0.5),1*(self.frame.size.height*0.5));
    CGContextScaleCTM(context, kMagnifyScale,kMagnifyScale);
    CGContextTranslateCTM(context,-1*(touchPoint.x),-1*(touchPoint.y));
    
    [self.viewToMagnify.layer renderInContext:context];
    
    CGContextRestoreGState(context);
    [glass drawInRect: bounds];
}

@end
