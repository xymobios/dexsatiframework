//
//  DisplayViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "DisplayViewController.h"
#import "UIImage+SimpleResize.h"
#import "CellFactory.h"
#import "Utility.h"
#import "BackGroundImageViewCell.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "ConstantString.h"
#import "ImageUtility.h"

@interface DisplayViewController()

@end

@implementation DisplayViewController
@synthesize library;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeView];
    [self setCroppedImage];
    [self setBackGroundImages];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

#pragma mark- Table View  delegate and call

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayOfImages.count;
}
- (NSString*)kindOfCellAtIndexPath:(NSIndexPath*)indexPath
{
    return @"CellForCroppedImage";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[CellFactory sharedCellFactory] frameForCellOfKind:[self kindOfCellAtIndexPath:indexPath]].size.height;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return [[CellFactory sharedCellFactory] cellOfKind:[self kindOfCellAtIndexPath:indexPath]
                                              forTable:tableView
                                        withBackground:nil];
}


- (void)tableView:(UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    [self prepareCell:cell ForTeamSelection:[arrayOfImages objectAtIndex:indexPath.row]];
    
}

- (void)prepareCell:(UITableViewCell*)cell ForTeamSelection:(UIImage *)cropImage
{
    UIImageView *picture = (UIImageView*) [cell viewWithTag:1];
    picture.image = cropImage;
    picture.layer.masksToBounds = YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog( @"File Path %@",[arrayOfImages objectAtIndex:indexPath.row]);
    
    UIImageView *gestureImage = [[UIImageView alloc] init];
    [gestureImage setFrame:CGRectMake([Utility getXpoint:stickerView.frame.size.width frameWidth:[Utility deviceWidth]],[Utility getYpointForViewController:stickerView.frame.size.height frameHeight:[Utility deviceHeight]], stickerView.frame.size.width,stickerView.frame.size.height)];
    
    gestureImage.image = [[arrayOfImages objectAtIndex:indexPath.row] imageByScalingToSize:stickerView.frame.size contentMode:UIViewContentModeScaleAspectFit];
    
    /*
    [gestureImage setFrame:CGRectMake([Utility getXpoint:stickerView.frame.size.width frameWidth:[Utility deviceWidth]],[Utility getYpointForViewController:stickerView.frame.size.height frameHeight:[Utility deviceHeight]], gestureImage.frame.size.width,stickerView.frame.size.height)];
    */
    gestureImage.userInteractionEnabled = YES;
   
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
    [panGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:panGestureRecognizer];
    // create and configure the rotation gesture
   
    UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
    [rotationGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:rotationGestureRecognizer];
    
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureDetected:)];
    [pinchGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:pinchGestureRecognizer];
    [self.view addSubview:gestureImage];
    isSelectCropImage = YES;
    [self hideImageTableView];
}

#pragma mark- Collection View  delegate and call
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return backGroundImageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BackGroundImageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    [self prepareCollectionCell:cell forImage: [backGroundImageArray  objectAtIndex:indexPath.row]  indexPath: indexPath];
    
        return cell;
}
- (void) prepareCollectionCell:(BackGroundImageViewCell*)cell forImage:(NSString*)bgImage indexPath: (NSIndexPath *) indexPath
{
    cell.backgroundImage.image = nil;
    cell.backgroundImage.image = [UIImage imageNamed:bgImage];
    //cell.backgroundImage.image =[[UIImage imageNamed:bgImage] imageByScalingToSize:cell.backgroundImage.frame.size contentMode:UIViewContentModeScaleAspectFit];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _backGroundImage.image = [UIImage imageNamed:[backGroundImageArray objectAtIndex:indexPath.row]];
}

#pragma mark- Gesture delegate and call

- (void)handlePinch:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    //handle pinch...
}

-(void)handlePanGesture:(id)sender
{
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    [[sender view] setCenter:translatedPoint];
}
-(void) tapped:(UIGestureRecognizer *) sender
{
    CGFloat r=(CGFloat) random()/(CGFloat) RAND_MAX;
    CGFloat g=(CGFloat) random()/(CGFloat) RAND_MAX;
    CGFloat b=(CGFloat) random()/(CGFloat) RAND_MAX;
    stickerView.backgroundColor=[UIColor colorWithRed:r green:g blue:b alpha:1];
}


- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        [recognizer.view setTransform:CGAffineTransformTranslate(recognizer.view.transform, translation.x, translation.y)];
        [recognizer setTranslation:CGPointZero inView:recognizer.view];
        [self.view  bringSubviewToFront:recognizer.view];
    }
}
-(void) rotation:(UIRotationGestureRecognizer *) sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged) {
        [sender view].transform = CGAffineTransformRotate([[sender view] transform], [(UIRotationGestureRecognizer *)sender rotation]);
        [(UIRotationGestureRecognizer *)sender setRotation:0];}
}

-(void) pinchGestureDetected:(UIPinchGestureRecognizer *) sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged)
    {
        [sender view].transform = CGAffineTransformScale([[sender view] transform], [sender scale], [sender scale]);
        [sender setScale:1];    }
}


-(void) longPressed:(UIGestureRecognizer *) sender
{
    DebugLog(@"LongPressed");
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void) stop: sender
{
    stickerView.backgroundColor=[UIColor blueColor];
}

#pragma mark- IBActions

- (IBAction)showImageTableView
{
    if(arrayOfImages.count == 0)
    {
        stickerViewContainer.hidden = YES;
        imageTable.hidden = YES;
        return;
    }
    imageTable.hidden = !imageTable.hidden;
    stickerViewContainer.hidden = !stickerViewContainer.hidden;
    if(!imageTable.hidden)
    {
        
//        [UIView animateWithDuration:0.5
//                              delay:0.1
//                            options: UIViewAnimationOptionCurveEaseInOut
//                         animations:^
//         {
//             CGRect frame = stickerViewContainer.frame;
//             //frame.origin.y = 0;
//             frame.origin.x = [Utility deviceWidth] - 200;
//             stickerViewContainer.frame = frame;
//         }
//                         completion:^(BOOL finished)
//         {
//             NSLog(@"Completed");
//             
//         }];

        
        [imageTable reloadData];
    }
}
- (void)hideImageTableView
{
//    [UIView animateWithDuration:0.5
//                          delay:0.1
//                        options: UIViewAnimationOptionCurveEaseInOut
//                     animations:^
//     {
//         CGRect frame = stickerViewContainer.frame;
//         //frame.origin.y = 0;
//         frame.origin.x = [Utility deviceWidth]+200;
//         stickerViewContainer.frame = frame;
//     }
//                     completion:^(BOOL finished)
//     {
//         NSLog(@"Completed");
//         
//     }];
    
    imageTable.hidden = YES;
    stickerViewContainer.hidden = YES;
}
- (IBAction)backGroundImageCollection
{
    backGroundImageViewContainer.hidden = !backGroundImageViewContainer.hidden;
    if(!backGroundImageViewContainer.hidden)
    {
        [backgroundImageCollectionView reloadData];
    }
}
- (IBAction)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)savedBackGroundImage
{
    if(!isSelectCropImage)
    {
        [Utility showAlertViewWithTitle:@""
                                  message:kSelectImageMsg
                                 delegate:nil
                             buttonTitles:[NSArray arrayWithObject:kOK]
                        cancelButtonIndex:0 tagValue:-1];
         return;
    }
    
    [self hideActions];
    [self hideImageTableView];
    backGroundImageViewContainer.hidden = YES;
    [self.library saveImage:[self captureView] toAlbum:[Utility appName] withCompletionBlock:^(NSError *error) {
        if (error!=nil) {
            NSLog(@"error: %@", [error description]);
        }
    }];    
    [ImageUtility saveImage:[self captureView] savedPath:kCreationPath imageInfo:nil];
    [self showActions];
}

#pragma mark- User Define Methods
- (void)initializeView
{
    // Saved Image in Gallery to use this object
    self.library = [[ALAssetsLibrary alloc] init];
    [Utility resizeViewForiPhoneAndiPod:self.view];
    [Utility resizeViewForiPhoneAndiPod:_backGroundImage];
    stickerViewContainer.layer.cornerRadius = 6;
}

- (void) setCroppedImage
{    arrayOfImages = [NSMutableArray array];
    NSArray *imageInfo =  [ImageUtility getListOfSavedImage:YES path:kCroping];
    for(int image = 0; image< imageInfo.count ;image++)
    {
        [arrayOfImages addObject:[[imageInfo objectAtIndex:image] valueForKey:kImageData]];
    }    
    if(arrayOfImages.count == 0)
    {
        stickerViewContainer.hidden = YES;
        imageTable.hidden = YES;
    }
    else{
        stickerViewContainer.hidden = NO;
        [imageTable reloadData];
    }
    [stickerViewContainer setFrame:CGRectMake(stickerViewContainer.frame.origin.x,stickerViewContainer.frame.origin.y, stickerViewContainer.frame.size.width, [Utility deviceHeight] - stickerViewContainer.frame.origin.y-backGroundImageViewContainer.frame.size.height -20)];
   DebugLog(@"imageInfo %@", [imageInfo valueForKey:kImageName]);
}
- (void)setBackGroundImages
{
    backGroundImageViewContainer.layer.cornerRadius = 6;
    backGroundImageArray = [NSArray arrayWithObjects:@"BG1.PNG",@"BG2.PNG",@"BG3.PNG",@"BG4.PNG",@"BG5.PNG",@"BG6.PNG",@"BG7.PNG",nil];
    _backGroundImage.image = [UIImage imageNamed:[backGroundImageArray objectAtIndex:0]];
    
    [backgroundImageCollectionView registerNib:[UINib nibWithNibName:@"BackGroundImageViewCell" bundle:nil]
                    forCellWithReuseIdentifier:@"CELL"];
    [backGroundImageViewContainer setFrame:CGRectMake(100,[Utility deviceHeight]-backGroundImageViewContainer.frame.size.height-6, [Utility deviceWidth] -110, backGroundImageViewContainer.frame.size.height)];
    [self.view addSubview:backGroundImageViewContainer];
}

- (UIImage *)captureView
{
    //hide controls if needed
    CGRect rect = [self.view bounds];    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.view.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (void)hideActions
{
    saveButton.hidden = YES;
    backGroundButton.hidden = YES;
    cropButton.hidden = YES;
    backButon.hidden = YES;
}
- (void)showActions
{
    saveButton.hidden = NO;
    backGroundButton.hidden = NO;
    cropButton.hidden = NO;
    backButon.hidden = NO;
}


@end
