//
//  MainViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "MainViewController.h"
#import "CropperViewController.h"
#import "DisplayViewController.h"
#import "Utility.h"
#import "YourCreationViewController.h"
#import "PastePhotoViewController.h"
#import "SelectAdvanceEditController.h"
#import "SettingViewController.h"


@interface MainViewController ()

@end

@implementation MainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    self.title = @"Cut Paste";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cropButtonClikced:(id)sender
{   
    CropperViewController *crop = [[CropperViewController alloc] init];
    [crop selectPicture];
    [self.navigationController pushViewController:crop animated:YES];
}
// Create Collage in Android
- (IBAction)displayButtonClikced:(id)sender
{
    if([[ImageUtility getListOfSavedImage:YES path:kCroping] count] == 0)
    {
        [Utility showAlertViewWithTitle:@""
                                message:kCollageAndPasteEmptyMSg
                               delegate:nil
                           buttonTitles:[NSArray arrayWithObject:kOK]
                      cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
        
        
        return;
    }
    
    DisplayViewController *display = [[DisplayViewController alloc] init];
    [self.navigationController pushViewController:display animated:YES];
}
// Your Creation in Android app
- (IBAction)yourCreationClikced:(id)sender
{
    
    if([[ImageUtility getListOfSavedImage:YES path:kCreationPath] count] == 0)
    {
        [Utility showAlertViewWithTitle:@""
                                message:kCreationemptyMsg
                               delegate:nil
                           buttonTitles:[NSArray arrayWithObject:kOK]
                      cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];

        
        return;
    }
    YourCreationViewController *display = [[YourCreationViewController alloc] init];
    [self.navigationController pushViewController:display animated:YES];
}

- (IBAction)photoPasteController:(id)sender
{
    
    if([[ImageUtility getListOfSavedImage:YES path:kCroping] count] == 0)
    {
        [Utility showAlertViewWithTitle:@""
                                message:kCollageAndPasteEmptyMSg
                               delegate:nil
                           buttonTitles:[NSArray arrayWithObject:kOK]
                      cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
        
        
        return;
    }

    PastePhotoViewController *photoPaste = [[PastePhotoViewController alloc] init];
    [photoPaste selectPicture];
    [self.navigationController pushViewController:photoPaste animated:YES];
}

- (IBAction)advanceEditClick:(id)sender
{    
    if([[ImageUtility getListOfSavedImage:YES path:kCroping] count] == 0)
    {
        [Utility showAlertViewWithTitle:@""
                                message:kAdvaceEditEmptyMSg
                               delegate:nil
                           buttonTitles:[NSArray arrayWithObject:kOK]
                      cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
        
        
        return;
    }
    
    SelectAdvanceEditController *advaceEdit = [[SelectAdvanceEditController alloc] init];
    [self.navigationController pushViewController:advaceEdit animated:YES];
}

- (IBAction)settingButtonClicked:(id)sender
{
    SettingViewController *setting = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:setting animated:YES];
}




@end
