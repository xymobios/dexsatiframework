//
//  FullImageViewContoller.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "FullImageViewContoller.h"
#import "ImageUtility.h"
#import "ConstantString.h"

@interface FullImageViewContoller ()
 @property(nonatomic, strong) IBOutlet UIImageView *fullImage;
@end

@implementation FullImageViewContoller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *correctImageSize = [_createdImageInfo objectForKey:kImageData];
    
    correctImageSize = [correctImageSize imageByScalingToSize:_fullImage.frame.size contentMode:UIViewContentModeScaleAspectFit];   
    
//     CGRect imageRect = CGRectMake([Utility getXpoint:correctImageSize.size.width frameWidth:_fullImage.frame.size.width] , [Utility getYpointForViewController:correctImageSize.size.height frameHeight:_fullImage.frame.size.height +_fullImage.frame.origin.y],correctImageSize.size.width, correctImageSize.size.height);
    
//    _fullImage.frame = imageRect;
    _fullImage.image = correctImageSize;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteButtonClicked
{
    [ImageUtility removeCachedImageForKey:[_createdImageInfo objectForKey:kImageKey] path:kCreationPath];
    [self backButtonClick];
}
- (IBAction)shareButtonClicked
{
    UIImage * img = [_createdImageInfo objectForKey:kImageData];
    NSArray * shareItems = @[kShareContent, img];
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
}

@end
