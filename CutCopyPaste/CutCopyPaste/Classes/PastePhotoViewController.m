//
//  PastePhotoViewController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 02/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "PastePhotoViewController.h"
#import "Utility.h"
#import "ConstantString.h"


@interface PastePhotoViewController ()

@end

@implementation PastePhotoViewController

- (void)viewDidLoad {
    controller = [[CameraGalleryController alloc] init];
    [super initializeView];
    [self hideActions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonClicked {
    [GetAppDelegate() hideFadeView];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark- Camera And  Gallery delegate
- (void) mediaError:(NSString *) errorMessage errorCode:(NSInteger) code
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:errorMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    [myAlertView show];
    [self backButtonClicked];
    return;
}
- (void)cameraPermissionGranted
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)galleryPermissionGranted
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - IBActions
- (IBAction)takePhoto:(UIButton *)sender
{
    [GetAppDelegate() showFadeView];
    [controller takePhotoFromCamera:self];
}
- (IBAction)selectPhoto:(UIButton *)sender
{
    [controller takePhotoFromGallery:self];
}
- (void)selectPicture
{    
    [Utility showAlertViewWithTitle:@""
                            message:@"Select Picture"
                           delegate:self
                       buttonTitles:[NSArray arrayWithObjects:kCancel,@"Camera",@"Gallery", nil]
                  cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
}

#pragma mark- Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    //
    [super showActions];
    [super setCroppedImage];
    
    self.backGroundImage.image = [chosenImage imageByScalingToSize:self.backGroundImage.frame.size contentMode:UIViewContentModeScaleAspectFit];
    [self.view  sendSubviewToBack:self.backGroundImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self backButtonClicked];
}

#pragma mark- AlertView delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [fadeView removeFromSuperview];
    DebugLog(@"buttonIndex %ld",(long)buttonIndex);
    
    if(ENUM_TAG_PICTURE_SET == alertView.tag)
    {
        if(buttonIndex == 1)
        {
            [self takePhoto:nil];
        }
        else if(buttonIndex == 2)
        {
            [self selectPhoto:nil];
        }
        else{
            [self backButtonClicked];
            DebugLog(@"Cancel Selection");
        }
    }
}
- (void)showActions
{
    saveButton.hidden = NO;
    backGroundButton.hidden = NO;
    cropButton.hidden = NO;
    backButon.hidden = NO;
}
@end
