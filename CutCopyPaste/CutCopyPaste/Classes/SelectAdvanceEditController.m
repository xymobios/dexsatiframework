//
//  SelectAdvanceEditController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "SelectAdvanceEditController.h"
#import "AdvanceEditCell.h"
#import "Utility.h"
#import "AdvaceEditController.h"

@interface SelectAdvanceEditController ()

@end

@implementation SelectAdvanceEditController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [croppedImageCollectionView registerNib:[UINib nibWithNibName:@"AdvanceEditCell" bundle:nil]
    forCellWithReuseIdentifier:@"ADVANCE_EDIT_CELL"];  
   
}

- (void) viewWillAppear:(BOOL)animated
{
    croppedImages =  [ImageUtility getListOfSavedImage:YES path:kCroping];
    if(croppedImages.count == 0)
    {
        [self backButtonClick];
        return;
    }
    DebugLog(@"image Info %@",croppedImages);
    [croppedImageCollectionView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- IBAction
- (IBAction)backButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Collection View  delegate and call
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return croppedImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AdvanceEditCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ADVANCE_EDIT_CELL" forIndexPath:indexPath];
    
    // [self prepareCollectionCell:cell forImage: [createdImage  objectAtIndex:indexPath.row]  indexPath: indexPath];
    
    [self prepareCollectionCell:cell forDictionary: [croppedImages  objectAtIndex:indexPath.row]  indexPath: indexPath];
    return cell;
}
//- (void) prepareCollectionCell:(YourCreationViewCell*)cell forImage:(NSString*)bgImage indexPath: (NSIndexPath *) indexPath
//{
////    ALAsset *asset = createdImage[indexPath.row];
////    CGImageRef thumbnailImageRef = [asset thumbnail];
////    UIImage *thumbnail = [UIImage imageWithCGImage:thumbnailImageRef];
////    cell.backgroundImage.image = nil;
////    cell.backgroundImage.image = thumbnail;
//
//    cell.backgroundImage.image =createdImage[indexPath.row] ;
//}

- (void) prepareCollectionCell:(AdvanceEditCell*)cell forDictionary:(NSDictionary*)imageInfo indexPath: (NSIndexPath *) indexPath
{
    cell.backgroundImage.image = [imageInfo valueForKey:kImageData];
    cell.imageName.text = [imageInfo valueForKey:kImageName];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake([Utility deviceWidth]/2 -10, [Utility deviceWidth]/2-10 + 20);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 7, 0, 7);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[GetAppDelegate() showLoadingViewWithMsg:@""];
    AdvaceEditController *advanceEditor = [[AdvaceEditController alloc] init];
      advanceEditor.cropedImageInfo = [croppedImages  objectAtIndex:indexPath.row];
    [advanceEditor setLaunchScreen:ENUM_OPEN_SELECT_SCREEN];
    [self.navigationController pushViewController:advanceEditor animated:YES];    
    //[GetAppDelegate() hideLoadingView];
    
}

@end
