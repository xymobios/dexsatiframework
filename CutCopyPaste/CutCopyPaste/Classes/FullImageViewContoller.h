//
//  FullImageViewContoller.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullImageViewContoller : UIViewController

@property(nonatomic, strong) NSDictionary *createdImageInfo;

@end
