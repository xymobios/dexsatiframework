//
//  SettingViewController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 12/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCColorPickerViewController.h"

@interface SettingViewController : UIViewController<FCColorPickerViewControllerDelegate>
//@property (nonatomic, assign) UIColor *currentColor;
@property (nonatomic, assign) UIColor *selectedColor;
 
@end
