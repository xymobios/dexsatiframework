//
//  AdvaceEditController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "AdvaceEditController.h"
#import "Utility.h"
#import "AdvanceEditView.h"

@interface AdvaceEditController ()
{
    
    
}
@property (nonatomic , strong) IBOutlet UIImageView *advaceCroppedImage;
@property (strong,nonatomic)  AdvanceEditView *editView;
@end

@implementation AdvaceEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utility resizeViewForiPhoneAndiPod:self.view];
    
    UIImage *cropPics = [_cropedImageInfo objectForKey:kImageData];
    imageName = [_cropedImageInfo objectForKey:kImageKey];
    // Increaes 8 % of size of image
    CGSize cropImageSize = CGSizeMake(cropPics.size.width *8, cropPics.size.height*8);
    
    //Get the new image with increaed size.
    cropImage = [cropPics imageByScalingToFillSize:cropImageSize];
    
    // Set the image with specific ratio.
    cropImage = [cropImage imageByScalingToSize:_advaceCroppedImage.frame.size contentMode:UIViewContentModeScaleAspectFit];
    CGRect viewFrame=self.view.frame;
    self.editView=[[AdvanceEditView alloc]initWithFrame:viewFrame];
    
    if(_launchScreen == ENUM_OPEN_FOROM_CUT_SCREEN)
    {
        [self openWithEditMode];
    }
    else
    {
        [self loadScreen];
    }  
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- IBAction
- (void) loadScreen
{
    _advaceCroppedImage.image = cropImage;
    [self.editView setStatus:ENUM_NOT_DRAW];
    [self.view addSubview: firstView];
    [self.view sendSubviewToBack:firstView];
}
- (void)openWithEditMode
{
    
    [self editButtonClick];
}

- (IBAction)backButtonClick
{
    if(_launchScreen == ENUM_OPEN_FOROM_CUT_SCREEN)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)editButtonClick
{
    //zoomDeleteView.hidden = NO;
    
    saveButton.hidden = NO;
    topButtonView.hidden = NO;
    edit.hidden = YES;
    share.hidden = YES;
    deleteImage.hidden = YES;
    
    [firstView removeFromSuperview];
    
    self.editView.advanceImage = cropImage;
    [self.editView setStatus:ENUM_IMAGE_DRAW];
    [self.view addSubview: self.editView];
    [self.view sendSubviewToBack:self.editView];
}
- (IBAction)deleteImageVuttonClick
{
    
}
- (IBAction)shareButtonClick
{
    UIImage * img = [_cropedImageInfo objectForKey:kImageData];
    NSArray * shareItems = @[kShareContent, img];
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void) pinchGestureDetected:(UIPinchGestureRecognizer *) sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged)
    {
        [sender view].transform = CGAffineTransformScale([[sender view] transform], [sender scale], [sender scale]);
        [sender setScale:1];    }
}
- (IBAction)deleteButtonClick
{
    
    //* Below code is r&d may be it will be use in future*//
    //    zoomDeleteView.hidden = NO;
    //    saveButton.hidden = NO;
    //    topButtonView.hidden = NO;
    //    edit.hidden = YES;
    //    share.hidden = YES;
    //    deleteImage.hidden = YES;
    //
    //    [firstView removeFromSuperview];
    //
    //    cropImage = [self getCurrentImage];
    //
    //    self.editView.advanceImage = cropImage;
    //    [self.editView setStatus:ENUM_IMAGE_DRAW];
    //    [self.view addSubview: self.editView];
    //    [self.view sendSubviewToBack:self.editView];
}


- (IBAction)zoomButtonClick
{
    
    //* Below code is r&d may be it will be use in future*//
    
    //    topButtonView.hidden = YES;
    //    saveButton.hidden = NO;
    //    UIImage * editImage = [self getCurrentImage];
    //    [_editView clearContext];
    //
    //    _advaceCroppedImage.image  = editImage;
    //
    //   // zoomView.image = editImage;
    //    _advaceCroppedImage.userInteractionEnabled = YES;
    //
    //    [_advaceCroppedImage setFrame:CGRectMake([Utility getXpoint:editImage.size.width frameWidth:[Utility deviceWidth]],[Utility getYpointForViewController:editImage.size.height frameHeight:[Utility deviceHeight]], editImage.size.width,editImage.size.height)];
    //
    //
    //
    //    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureDetected:)];
    //    [pinchGestureRecognizer setDelegate:self];
    //    [_advaceCroppedImage addGestureRecognizer:pinchGestureRecognizer];
    //
    //
    //    [_editView addSubview: firstView];
    //  //  [self.view bringSubviewToFront:zoomView];
    
    
}

- (IBAction)redoButtonClick
{
    lineWidthSlider.hidden = YES;
    [self.editView redoOperation];
}
- (IBAction)undoButtonClick
{
    lineWidthSlider.hidden = YES;
    [self.editView undoOperation];
}

- (IBAction)pencilButtonClicked
{
    if(lineWidthSlider.hidden)
    {
        lineWidthSlider.hidden = NO;
    }
}
- (IBAction)saveButtonClick
{
    UIImage * img = [self getCurrentImage];
    
    [ImageUtility saveImage:img savedPath:kCroping imageInfo:imageName];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
//Before Saving Hide all the UI Component
- (void)hideAlUIcomponent
{
    
}

- (IBAction)sliderChanged:(id)sender {
    
    UISlider * changedSlider = (UISlider*)sender;
    if(changedSlider == lineWidthSlider)
    {
        [self.editView setSliderValue:[NSNumber numberWithInt:lineWidthSlider.value]];
    }
    
}


- (UIImage *) getCurrentImage
{
    [_editView setBackgroundColor:[UIColor clearColor]];
    UIGraphicsBeginImageContextWithOptions(_editView.bounds.size, NO, 0.0);
    [_editView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return  img;
    
}


@end
