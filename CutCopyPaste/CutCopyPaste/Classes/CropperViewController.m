//
//  CropperViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CropperViewController.h"
#import "CropperView.h"
#import "Utility.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+SimpleResize.h"
#import "ImageUtility.h"
#import "SelectAdvanceEditController.h"
#import "AdvaceEditController.h"

@interface CropperViewController ()

@end

@implementation CropperViewController

- (void)viewDidLoad {
    
    [Utility resizeViewForiPhoneAndiPod:_fadeView];
    [Utility resizeViewForiPhoneAndiPod:self.view];
    controller = [[CameraGalleryController alloc] init];
    [super viewDidLoad];
     self.title = @"Crop Here";
     _cropView = [[CropperView alloc] initWithFrame:CGRectMake([Utility getXpoint:[Utility deviceWidth] frameWidth:[Utility deviceWidth]] , 77,[Utility deviceWidth], [Utility deviceWidth])];
    _cropView.userInteractionEnabled = YES;
    _cropView.contentMode = UIViewContentModeScaleAspectFit;
     _cropView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma marks- IBAction

- (IBAction)backButtonClicked
{
    [GetAppDelegate() hideFadeView];
    [_fadeView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark- Camera And  Gallery delegate
- (void) mediaError:(NSString *) errorMessage errorCode:(NSInteger) code
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:errorMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    [myAlertView show];
    [self backButtonClicked];
    return;
}
- (void)cameraPermissionGranted
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)galleryPermissionGranted
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - IBActions
- (IBAction)takePhoto:(UIButton *)sender
{
    [GetAppDelegate() showFadeView];
    [controller takePhotoFromCamera:self];
}
- (IBAction)selectPhoto:(UIButton *)sender
{
    //[self.view addSubview:_fadeView];
    //[Utility resizeViewForiPhoneAndiPod:_fadeView];
    [GetAppDelegate() showFadeView];
    [controller takePhotoFromGallery:self];
}
- (void)selectPicture
{
    //[self.view addSubview:_fadeView];
    [GetAppDelegate() showFadeView];
    [Utility showAlertViewWithTitle:@""
                            message:@"Select Picture"
                           delegate:self
                       buttonTitles:[NSArray arrayWithObjects:kCancel,@"Camera",@"Gallery", nil]
                  cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
}

#pragma mark- Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    pickerImage = info[UIImagePickerControllerEditedImage];
    [_cropView clear];
    [_cropView removeFromSuperview];
    
  // Reset Frame for Crop View .
    CGRect orignalSize = CGRectMake([Utility getXpoint:[Utility deviceWidth] frameWidth:[Utility deviceWidth]] , 77,[Utility deviceWidth], [Utility deviceWidth]);
    [_cropView setFrame:orignalSize];
    
     _cropView.controllerImage = [pickerImage imageByScalingToSize:_cropView.frame.size contentMode:UIViewContentModeScaleAspectFit];
    
    [_cropView setFrame:CGRectMake(_cropView.frame.origin.x, _cropView.frame.origin.y,  _cropView.controllerImage.size.width,   _cropView.controllerImage.size.height)];
    
    //_cropView.imageName = imageName;
    [_cropView setNewImage];
    
    [self.view addSubview:_cropView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self backButtonClicked];
}
#pragma mark- AlertView delegates 

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //[_fadeView removeFromSuperview];
    [GetAppDelegate() hideFadeView];
    DebugLog(@"buttonIndex %ld",(long)buttonIndex);
    
    if(ENUM_TAG_PICTURE_SET == alertView.tag)
    {
        if(buttonIndex == 1)
        {
            [self takePhoto:nil];
        }
        else if(buttonIndex == 2)
        {
            [self selectPhoto:nil];
        }
        else{
            [self backButtonClicked];
            DebugLog(@"Cancel Selection");
        }
    }
}
#pragma IBActions

- (IBAction)cancelClcik
{
    [_croppedImageView removeFromSuperview];
    [_fadeView removeFromSuperview];
    [GetAppDelegate() hideFadeView];
    [_cropView removeFromSuperview];
    [_cropView clear];
    _cropView.controllerImage = [pickerImage imageByScalingToSize:_cropView.frame.size contentMode:UIViewContentModeScaleAspectFit];
    [_cropView setNewImage];
    [self.view addSubview:_cropView];
    //[self backButtonClicked];
}
- (IBAction)doneClick
{
    [_fadeView removeFromSuperview];
    [GetAppDelegate() hideFadeView];
    [ImageUtility saveImage:_croppedImage savedPath:kCroping imageInfo:nil];
    [self backButtonClicked];
}
- (IBAction)advacneEdit
{
    NSString *imageName = [ImageUtility saveImage:_croppedImage savedPath:kCroping imageInfo:nil];
   // [ImageUtility getImageFromKey:imageName path:kCroping];
    AdvaceEditController *advanceEditor = [[AdvaceEditController alloc] init];
    
    advanceEditor.cropedImageInfo = [ImageUtility getImageFromKey:imageName imageData:_croppedImage];
    [advanceEditor setLaunchScreen:ENUM_OPEN_FOROM_CUT_SCREEN];
    [self.navigationController pushViewController:advanceEditor animated:YES];
}

#pragma mark - Call Back Delegate after clipped image.

- (void)croppedImageWithPath:(CGPathRef)path image:(UIImage *)image
{     //_cropView.orignalImage = image;
    
    
    [self.view addSubview:_fadeView];
    //[GetAppDelegate() showFadeView];
    NSLog(@"croppedImageWithPath");
    [_cropView removeFromSuperview];
    ///[_cropView setCliipedImage];
    [self.view addSubview:_cropView];
    
    [_croppedImageView setFrame:CGRectMake([Utility getXpoint:_croppedImageView.frame.size.width frameWidth:[Utility deviceWidth]],
                                           [Utility getYpointForViewController:_croppedImageView.frame.size.height frameHeight:[Utility deviceHeight]],_croppedImageView.frame.size.width, _croppedImageView.frame.size.height)];
    _croppedImage = image;
    _cropedImageContainer.image = image;
    [self.view addSubview:_croppedImageView];
}



@end