//
//  AdvanceEditCell.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvanceEditCell : UICollectionViewCell
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, retain) IBOutlet UILabel *imageName; //item share Button
@end
