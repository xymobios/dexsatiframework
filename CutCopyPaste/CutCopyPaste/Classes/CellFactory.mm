//
//  CellFactory.m
//
//  Created by Andrew Karpiuk on 4/21/09.
//  Copyright 2009 DAVA Consulting. All rights reserved.
//

#import "CellFactory.h"
#import "Utility.h"

static CellFactory *cellFactoryInstance = nil;

@implementation CellFactory

#pragma mark memory management

- (id)initWithNib: (NSString*)nibName  
{  
    if (self = [super init])
	{  
		frameStore			= [[NSMutableDictionary alloc] init];
        cellStore			= [[NSMutableDictionary alloc] init];
        NSArray *templates	= [[NSBundle mainBundle] loadNibNamed: nibName owner: self options: nil];  
		
        for (id templatex in templates)
		{
            if ([templatex isKindOfClass: [UITableViewCell class]])
			{  
                UITableViewCell *cellTemplate	= (UITableViewCell*)templatex;  
                NSString *key					= cellTemplate.reuseIdentifier;  
                if (key)
				{
                    [cellStore setObject: [NSKeyedArchiver archivedDataWithRootObject: templatex] forKey: key];
                }
				else
				{  
                    DebugLog(@"CellFactory: Cell has no reuseIdentifier");
                }  
            }  
        }
    }  
	
    return self;  
}  

#pragma mark shared methods

+ (CellFactory*)sharedCellFactory
{
	return cellFactoryInstance ? cellFactoryInstance : (cellFactoryInstance = [[CellFactory alloc] initWithNib: @"CellFactory"]);
}

#pragma mark cell management

- (UITableViewCell*)cellOfKind:(NSString*)cellKind
                      forTable:(UITableView*)tableView
                withBackground:(NSString*)backgroundName
{  
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellKind];  
    if (!cell)
	{  
        NSData *cellData = [cellStore objectForKey: cellKind];  
        if (cellData)
		{  
            cell = [NSKeyedUnarchiver unarchiveObjectWithData: cellData];  
			
			if (backgroundName)
			{
				UIImage *backgroundImage	= [UIImage imageNamed: backgroundName];
				cell.backgroundView			= [[UIImageView alloc] initWithImage: backgroundImage];
			}
        }
		else
		{  
            DebugLog(@"CellFactory: Don't know nothing about cell of kind %@", cellKind);  
        }  
    }  
	
    return cell;  
}

- (CGRect)frameForCellOfKind: (NSString*)cellKind
{
	NSData *frameData = [frameStore objectForKey: cellKind];  
	if (frameData)
	{  
		CGRect frame;
		[frameData getBytes: &frame length: sizeof(frame)];
		return frame;
	}
	else
	{
        NSData *cellData = [cellStore objectForKey: cellKind];  
        if (cellData)
		{  
            UITableViewCell *cell = [NSKeyedUnarchiver unarchiveObjectWithData: cellData];
			CGRect frame = cell.frame;
			
			[frameStore setObject: [NSData dataWithBytes: &frame length: sizeof(frame)] forKey: cellKind];
			
			return frame;
        }
		else
		{  
            DebugLog(@"CellFactory: Don't know nothing about cell of kind %@", cellKind);  
        }  
	}
	
	return CGRectNull;
}

@end
