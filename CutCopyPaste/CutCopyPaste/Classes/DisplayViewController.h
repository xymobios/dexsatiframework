//
//  DisplayViewController.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>




@interface DisplayViewController : UIViewController <UIGestureRecognizerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *arrayOfImages;
    IBOutlet UITableView *imageTable;
    IBOutlet UIView *stickerViewContainer;
    float firstX;
    float firstY;
    IBOutlet UIView *stickerView; 
    BOOL isCropViewInTop;

    IBOutlet UIView *backGroundImageViewContainer;
    IBOutlet UICollectionView *backgroundImageCollectionView;
    NSArray *backGroundImageArray;
    IBOutlet UIButton *backButon, *saveButton, *cropButton, *backGroundButton;
    
    BOOL isSelectCropImage; // for saving the image atleast one image should be selected by user , this is checked that crop image is selected or not.
    
}
@property(nonatomic, strong)IBOutlet UIImageView *backGroundImage;
@property (strong, atomic) ALAssetsLibrary* library;

- (void)initializeView;
- (void)hideActions;
- (void)showActions;
- (void) setCroppedImage;


@end
