//
//  YourCreationViewController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 05/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "YourCreationViewController.h"
#import "YourCreationViewCell.h"
#import "ConstantString.h"
#import "Utility.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "ImageUtility.h"
#import "FullImageViewContoller.h"

@interface YourCreationViewController()

@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *groups;

@end

@implementation YourCreationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [creationViews registerNib:[UINib nibWithNibName:@"YourCreationViewCell" bundle:nil]
                    forCellWithReuseIdentifier:@"CREATION_CELL"];
}

- (void) viewWillAppear:(BOOL)animated
{
    createdImage =  [ImageUtility getListOfSavedImage:YES path:kCreationPath];
    if(createdImage.count == 0)
    {
        [self backButtonClick];
        return;
    }
    DebugLog(@"image Info %@",createdImage);
    [creationViews reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Get the image from Galary and display in View Controller

- (void)initializationAssets
{
    
    if (self.assetsLibrary == nil) {
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    if (self.groups == nil) {
        _groups = [[NSMutableArray alloc] init];
    } else {
        [self.groups removeAllObjects];
    }
    [self getAlubumPhoto];
    
}

- (void) getAlubumPhoto
{
    
    createdImage = [NSMutableArray array];
    // setup our failure view controller in case enumerateGroupsWithTypes fails
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        
        NSString *errorMessage = nil;
        switch ([error code]) {
            case ALAssetsLibraryAccessUserDeniedError:
            case ALAssetsLibraryAccessGloballyDeniedError:
                //errorMessage = @"The user has declined access to it.";
              

                break;
            default:
                errorMessage = @"Reason unknown.";
                
                
                break;
        }
        
         Error Occure to get the photo from galary */
/*
        [Utility showAlertViewWithTitle:[Utility appName]
                                message:[error localizedDescription]
                               delegate:nil
                           buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                      cancelButtonIndex:0
                               tagValue:1];
        [self backButtonClick];
        
    };
    
    // emumerate through our groups and only add groups that contain photos
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
        [group setAssetsFilter:onlyPhotosFilter];
        
        
       if( ([[group valueForProperty:@"ALAssetsGroupPropertyType"] integerValue] ==ALAssetsGroupAlbum) &&
          [[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:[Utility appName]])
         {
             if ([group numberOfAssets] > 0)
             {
                 [self.groups addObject:group ];
                 [self loadImages];
             }
         }
    
        else
        {
            NSLog(@"Group Name %@",group);
        }
    };
    // enumerate only photos
    NSUInteger groupTypes = ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupFaces | ALAssetsGroupSavedPhotos;
    [self.assetsLibrary enumerateGroupsWithTypes:groupTypes usingBlock:listGroupBlock failureBlock:failureBlock];
    
}
-(void)loadImages
{
    ALAssetsGroup *assetGroup = [self.groups objectAtIndex:0];
    [assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop)
     {
         if(result == nil)
         {
             return;
         }
         if (result)
         {
             [createdImage addObject:result];
         }
         
     }];
    [self reloadImages];
}

-(void)reloadImages
{
    [creationViews reloadData];
}

*/

#pragma mark- Collection View  delegate and call
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return createdImage.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YourCreationViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CREATION_CELL" forIndexPath:indexPath];
    
   // [self prepareCollectionCell:cell forImage: [createdImage  objectAtIndex:indexPath.row]  indexPath: indexPath];
    
    [self prepareCollectionCell:cell forDictionary: [createdImage  objectAtIndex:indexPath.row]  indexPath: indexPath];
     return cell;
}
//- (void) prepareCollectionCell:(YourCreationViewCell*)cell forImage:(NSString*)bgImage indexPath: (NSIndexPath *) indexPath
//{
////    ALAsset *asset = createdImage[indexPath.row];
////    CGImageRef thumbnailImageRef = [asset thumbnail];
////    UIImage *thumbnail = [UIImage imageWithCGImage:thumbnailImageRef];
////    cell.backgroundImage.image = nil;
////    cell.backgroundImage.image = thumbnail;
//    
//    cell.backgroundImage.image =createdImage[indexPath.row] ;
//}

- (void) prepareCollectionCell:(YourCreationViewCell*)cell forDictionary:(NSDictionary*)imageInfo indexPath: (NSIndexPath *) indexPath
{
    cell.backgroundImage.image = [imageInfo valueForKey:kImageData];
    cell.imageName.text = [imageInfo valueForKey:kImageName];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FullImageViewContoller *display = [[FullImageViewContoller alloc] init];
    display.createdImageInfo = [createdImage  objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:display animated:YES];
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake([Utility deviceWidth]/2 -10, [Utility deviceWidth]/2-10 + 20);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
   return UIEdgeInsetsMake(0, 7, 0, 7);
}


#pragma mark- IBAction

- (IBAction)backButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
