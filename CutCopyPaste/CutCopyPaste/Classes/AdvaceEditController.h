//
//  AdvaceEditController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AdvaceEditController : UIViewController
{
    IBOutlet UIView *zoomDeleteView, *topButtonView, *firstView;
    IBOutlet UIButton  *back, *redoButton, *undoButton, *pencilbutton, *saveButton;
    IBOutlet UIButton *deleteButton, *zoomButton;
    IBOutlet UIButton *deleteImage, *share, *edit;
    IBOutlet UISlider *lineWidthSlider;
    //IBOutlet UIImageView *zoomView;
    UIImage *cropImage;
    NSString *imageName;
}

@property(nonatomic, strong) NSDictionary *cropedImageInfo;
@property(nonatomic, assign) NSInteger launchScreen;
- (void)loadScreen;
- (void)openWithEditMode;

@end
