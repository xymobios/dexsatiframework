//
//  SettingViewController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 12/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "SettingViewController.h"
#import "Utility.h"
#import "FCColorPickerViewController.h"

@interface SettingViewController ()
{
    IBOutlet UISwitch *cutMagnifySwitch, *editMagnifySwitch;   
    IBOutlet UIView *currentColorView;
}
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    cutMagnifySwitch.on = [Utility cutMagnifyingGlassStatus];
    editMagnifySwitch.on = [Utility advanceEditMagnifyingGlassStatus];
    [currentColorView setBackgroundColor:[Utility getCurrentColor]];
    
    currentColorView.layer.cornerRadius = 9;
    currentColorView.layer.borderColor = [UIColor blueColor].CGColor;
    currentColorView.layer.borderWidth = 2;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) cutMagnifyingGlass:(id)sender
{
    NSInteger status = ENUM_MAGNIFYING_GLASS_CUT_NORMAL;
    //cutMagnifySwitch.on = !cutMagnifySwitch.on;
    if(cutMagnifySwitch.on)
    {
       status = ENUM_MAGNIFYING_GLASS_CUT_ON;
    }
    else
    {
        status = ENUM_MAGNIFYING_GLASS_CUT_OFF;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:status forKey:kCutMFGlass];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(IBAction) advanceEditMagnifyingGlass:(id)sender
{
   // editMagnifySwitch.on = !editMagnifySwitch.on;
    
    NSInteger status = ENUM_ADVANCE_MAGNIFYING_GLASS_NORMAL;
   
    if(editMagnifySwitch.on)
    {
        status = ENUM_ADVANCE_MAGNIFYING_GLASS_ON;
    }
    else
    {
        status = ENUM_ADVANCE_MAGNIFYING_GLASS_OFF;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:status forKey:kAdEditMFGlass];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (IBAction)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}



-(IBAction)chooseColor:(id)sender {
    FCColorPickerViewController *colorPicker = [FCColorPickerViewController colorPicker];
    //colorPicker.color = self.selectedColor;
    colorPicker.delegate = self;
    colorPicker.currentColor = [Utility getCurrentColor];
    
    [colorPicker setModalPresentationStyle:UIModalPresentationFormSheet];
    [self presentViewController:colorPicker animated:YES completion:nil];
}

#pragma mark - FCColorPickerViewControllerDelegate Methods

-(void)colorPickerViewController:(FCColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color {
    
    [currentColorView setBackgroundColor:[Utility setCurrentColor:color]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)colorPickerViewControllerDidCancel:(FCColorPickerViewController *)colorPicker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
